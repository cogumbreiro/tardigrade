# Tardigrade: the little helping grader

Tardigrade is a tool that helps preparing, packaging, deploying and testing
programming assignments.

## Life-cycle of tardigrade

1. Build & test a container

    1. Setup files (copy files) to a build directory
    2. Build the container
    3. Run sanity tests within that container

2. Upload a container

3. Go to 1

## Developing an image

Lingo: an "image" is short for Docker container image.

A minimal directory contains:

- `manifest.yaml`
- `Dockerfile` (known as the build script)

The `Dockerfile` is used to build our image and uses files copied over from three
sources:
1. from the image template directory (this directory, files are listed in
`manifest.yaml` in `source` section)
2. from the assignment template directory
(see the assignment type section)
3. from the assignment directory.

The file `manifest.yaml` specifies three main concerns:
*  `source`: the list of files to copied to a build directory, which are then accessible to the container's build script (the `Dockerfile`) and to the setup script which is given in the assignment type
* `image_repository`: the image repository used to store the container image, see DockerHub. A grader server will pull our assignment from this repository. We can use a special variable called `${target}` to refer to the assignment name.
* `tests` specifies how to perform a system test within the image, so that we can double-check the built image makes sense.

### Testing section

Special variables: `${test_dir}` temporary directory created at the host
(outside the image OS)

The `test` section contans 3 sub-sections:
* `volumes`: A list of volume pairs (see `docker run --help`). This is useful to send/retrieve files to/from the image.
* `ok`: Tests that pass. The `cmd` section is the command run inside the container. Set a script in the `post` that should 
* `fail`: Tests that should fail. See `ok`.
* `debug`: Invoked when a test fails

Example:
```
copy:
  - file1.sh
  - file2.txt


test:
  # Copy these files to the test directory
  copy:
    - setup.sh

  volumes:
    - ${test_dir}:/autograder/submission

  ok:
    cmd: bash /autograder/submission/debug.sh
    post: process-ok ${test_dir}/${test_file}

  fail:
    cmd: bash /autograder/submission/debug.sh
    post: process-fail ${test_dir}/${test_file}
    # Copy one of these failing examples
    files:
        - always-fails.txt

  debug:
    cmd: bash
    cwd: /autograder/source

setup_cmd: ${build_dir}/configure.sh

```


## Developing an assignment type

Five sections:

* `image`: the container image this assignment type targets
* `source`: list of files to be copied to the build script
* `test.ok`: (optional) the list of test files to be used as passing tests
* `test.fail`: (optional) the list of test files to be used as failing tests
* `setup_cmd`: the command run exactly before running docker build


```
image: base-image

source:
  - run_autograder
  - configure.sh

test:
  fail:
    - fail/example1.txt
    - fail/example2.txt

# Image repository used to store the container image
image_repository: username/image:tag-${target}

```


---

Video about real life tardigrades: https://www.youtube.com/watch?v=kux1j1ccsgg
